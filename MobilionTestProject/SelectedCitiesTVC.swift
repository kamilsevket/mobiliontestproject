//
//  ChoosenCityTVC.swift
//  MobilionTestProject
//
//  Created by Kamil Tolkido on 9.01.2021.
//

import UIKit

class SelectedCitiesTVC: UITableViewCell {
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var backgroundViewOfContent: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundViewOfContent.layer.applyCornerRadiusShadow()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
