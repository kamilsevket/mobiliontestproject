//
//  Clouds.swift
//  MobilionTestProject
//
//  Created by Kamil Tolkido on 9.01.2021.
//


import Foundation
import ObjectMapper

struct Clouds : Mappable {
	var all : Int?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		all <- map["all"]
	}

}
