//
//  BaseForecast.swift
//  MobilionTestProject
//
//  Created by Kamil Tolkido on 9.01.2021.
//


import Foundation
import ObjectMapper

struct BaseForecast : Mappable {
	var cod : String?
	var message : Int?
	var cnt : Int?
	var list : [List]?
	var city : CityForForecast?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		cod <- map["cod"]
		message <- map["message"]
		cnt <- map["cnt"]
		list <- map["list"]
		city <- map["city"]
	}

}
