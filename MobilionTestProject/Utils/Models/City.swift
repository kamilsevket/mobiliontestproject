//
//  City.swift
//  MobilionTestProject
//
//  Created by Kamil Tolkido on 9.01.2021.
//

import Foundation
import ObjectMapper

struct City : Mappable {
	var coord : Coord?
	var country : String?
	var id : Int?
	var name : String?
	var state : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		coord <- map["coord"]
		country <- map["country"]
		id <- map["id"]
		name <- map["name"]
		state <- map["state"]
	}

}
