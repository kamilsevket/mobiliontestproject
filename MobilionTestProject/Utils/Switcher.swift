//
//  Switcher.swift
//  TolkidoNewHope
//
//  Created by Kamil Tolkido on 9.01.2021.
//  Copyright © 2021 Kamil Tolkido. All rights reserved.
//

import Foundation
import UIKit

class Switcher {
    
    static func updateRootVC(){
        
        var rootVC : UIViewController?
    
        rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "oDy-3v-Gwx") as! MainNavigationController
        
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = rootVC
        
    }
}
