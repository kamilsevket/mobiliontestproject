//
//  WeeklyForecastViewModel.swift
//  MobilionTestProject
//
//  Created by Kamil Tolkido on 9.01.2021.
//

import Foundation

struct WeeklyForecastViewModel {
    var dayName = String()
    var maxTemp = Int()
    var minTemp = Int()
    var icon = String()
}
