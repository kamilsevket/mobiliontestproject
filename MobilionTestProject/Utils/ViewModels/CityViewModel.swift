//
//  CityViewModel.swift
//  MobilionTestProject
//
//  Created by Kamil Tolkido on 9.01.2021.
//

import Foundation

struct CityViewModel {
    var name = String()
    var id = Int()
}
