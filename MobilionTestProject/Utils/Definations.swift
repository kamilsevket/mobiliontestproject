//
//  Definations.swift
//  MobilionTestProject
//
//  Created by Kamil Tolkido on 9.01.2021.
//

import Foundation
import UIKit


class Definations {
    public static var ALL_CITIES = [CityViewModel]()
    public static var CHOOSEN_CITIES = [CityViewModel]()
    
    public static var CURRENT_FORECST: List?
    public static var ALL_FORECAST = [List]()
    public static var SELECTED_CITIES_CURRENT_FORECAST = [List]()
    
}
