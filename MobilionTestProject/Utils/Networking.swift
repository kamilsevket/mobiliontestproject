//
//  Networking.swift
//  MobilionTestProject
//
//  Created by Kamil Tolkido on 9.01.2021.
//

import Foundation
import Alamofire
import AlamofireObjectMapper


class Networking {
    
    private var cityURL = "https://weathercase-99549.firebaseio.com/.json"
    private var byIdURL = "api.openweathermap.org/data/2.5/forecast?id="
    private var apiKey  = "95b538768fb34b89d308cfe270cf57ac&units=metric"
    
    func getCityList(completionHandler: @escaping (Error?,[City]) -> ()) {
        AF.request(cityURL,
                   method: .get,
                   encoding: JSONEncoding.default
        ).validate().responseArray { (response: AFDataResponse<[City]>) in
            
            if let status = response.response?.statusCode {
                switch(status){
                case 200:
                    do {
                        let result = try response.result.get()
                        
                        for city in result {
                            let cityViewModel = CityViewModel(name: city.name ?? "NULL",id: city.id ?? -1)
                            Definations.ALL_CITIES.append(cityViewModel)
                        }
                        
                        completionHandler(nil,result)
                        print("Success")
                    }
                    catch {
                        fatalError("unable to convert data to JSON")
                    }
                default:
                    print("error with response status: \(status)")
                }
            }
        }
    }
    
    func getForecastById(cityId: String, completionHandler: @escaping (Error?,BaseForecast) -> ()) {
        let url = "https://" + byIdURL + cityId + "&lang=tr" + "&appid=" + apiKey
        print(url)
        AF.request(url,
                   method: .get,
                   encoding: JSONEncoding.default
        ).validate().responseObject { (response: AFDataResponse<BaseForecast>) in
            
            if let status = response.response?.statusCode {
                switch(status){
                case 200:
                    do {
                        let result = try response.result.get()
                        completionHandler(nil,result)
                        print("Success")
                    }
                    catch {
                        fatalError("unable to convert data to JSON")
                    }
                default:
                    print("error with response status: \(status)")
                }
            }
        }
    }

    
    
}
