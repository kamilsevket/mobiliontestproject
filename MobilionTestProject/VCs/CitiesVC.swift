//
//  CitiesVC.swift
//  MobilionTestProject
//
//  Created by Kamil Tolkido on 9.01.2021.
//

import UIKit

class CitiesVC: UIViewController, UISearchResultsUpdating {
    
    @IBOutlet weak var searchTV: UITableView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var citiesTV: UITableView!
    
    @IBOutlet weak var searchResultsScrollView: IKScrollView!
    
    let searchController = UISearchController(searchResultsController: nil)
    var searchResults = [CityViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        networking()
        UISetup()
    }
    
    
    fileprivate func UISetup() {
        searchResultsScrollView.isScrollEnabled = false
        searchController.searchResultsUpdater = self
        self.navigationItem.searchController?.searchBar.alpha = 0.0
        self.searchResultsScrollView.alpha = 0.0
    
        self.definesPresentationContext = true
        
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Vazgeç"
        // Don't hide the navigation bar because the search bar is in it.
        searchController.hidesNavigationBarDuringPresentation = false
        
 
        citiesTV.delegate = self
        citiesTV.dragDelegate = self
        citiesTV.dropDelegate = self
    }
    
    
//    MARK: Networking
    fileprivate func networking(){
        for city in Definations.CHOOSEN_CITIES {
            Networking().getForecastById(cityId: String(city.id)) { (err, forecast) in
                if err == nil {
                    Definations.SELECTED_CITIES_CURRENT_FORECAST.append(forecast.list![0])
                    self.citiesTV.reloadData()
                }
            }
        }
    }
    
//    MARK: Search Funcs
    
    fileprivate func filterContent(for searchText: String) {
        searchResults = Definations.ALL_CITIES.filter {
            let match =  $0.name.range(of: searchText)
            
            return (match != nil)
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        // If the search bar contains text, filter our data with the string
        if let searchText = searchController.searchBar.text {
            filterContent(for: searchText)
            // Reload the table view with the search result data.
            searchTV.reloadData()
        }
    }
    
    

    @IBAction func showSearchBar(_ sender: Any) {
        if(self.searchResultsScrollView.alpha == 0.0){
            UIView.animate(withDuration: 0.3) {
                self.editButton.setTitle("Geri", for: .normal)
                self.searchButton.alpha = 0.0
                self.navigationItem.titleView = self.searchController.searchBar
                self.navigationItem.searchController?.searchBar.alpha = 1.0
                self.searchResultsScrollView.alpha = 1.0
                
            }
        }
    }
    
    @IBAction func editCitiesTableView(_ sender: Any) {
        if(self.searchResultsScrollView.alpha == 0.0){
            if(self.citiesTV.isEditing == true){
                UIView.animate(withDuration: 0.5) {
                    self.citiesTV.dragInteractionEnabled = false
                    self.citiesTV.isEditing = false
                    self.editButton.setTitle("Düzenle", for: .normal)
                }
            }else{
                UIView.animate(withDuration: 0.5) {
                    self.citiesTV.dragInteractionEnabled = true
                    self.citiesTV.isEditing = true
                    self.editButton.setTitle("Bitti", for: .normal)
                }
            }
        }else{
            UIView.animate(withDuration: 0.3) {
                self.navigationItem.leftBarButtonItem?.isEnabled = true
                self.navigationItem.titleView = nil
                self.navigationItem.searchController?.searchBar.alpha = 0.0
                self.searchResultsScrollView.alpha = 0.0
                self.editButton.setTitle("Düzenle", for: .normal)
                self.searchButton.alpha = 1.0
            }
        }
        
    }
        
}


// MARK: UITableViewDelegate, DataSource, DragDelegate
extension CitiesVC: UITableViewDelegate, UITableViewDataSource, UITableViewDragDelegate, UITableViewDropDelegate {
    
    func tableView(_ tableView: UITableView, performDropWith coordinator: UITableViewDropCoordinator) {
        print("dropped")
    }
    
    
    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        
        let dragItem = UIDragItem(itemProvider: NSItemProvider())
        dragItem.localObject = Definations.SELECTED_CITIES_CURRENT_FORECAST[indexPath.row]
        return [ dragItem ]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == searchTV {
            return searchController.isActive ? searchResults.count : Definations.ALL_CITIES.count
        }else{
            return Definations.SELECTED_CITIES_CURRENT_FORECAST.count
        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        // Update the model
        let mover = Definations.SELECTED_CITIES_CURRENT_FORECAST.remove(at: sourceIndexPath.row)
        Definations.SELECTED_CITIES_CURRENT_FORECAST.insert(mover, at: destinationIndexPath.row)
        
        let moverForMainVC = Definations.CHOOSEN_CITIES.remove(at: sourceIndexPath.row)
        Definations.CHOOSEN_CITIES.insert(moverForMainVC, at: destinationIndexPath.row)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didCitiesChanged"), object: nil, userInfo: nil)
        
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == searchTV {
            let entry = searchController.isActive ?
                searchResults[indexPath.row] : Definations.ALL_CITIES[indexPath.row]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath)
            cell.textLabel?.text = entry.name
            cell.textLabel?.font = UIFont.textStyle8
            cell.textLabel?.textColor = UIColor.slate
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cityCell", for: indexPath) as! SelectedCitiesTVC
            
            if Definations.SELECTED_CITIES_CURRENT_FORECAST.count != 0 {
                
                cell.cityName.text = Definations.CHOOSEN_CITIES[indexPath.row].name
     
                cell.date.text = Definations.SELECTED_CITIES_CURRENT_FORECAST[indexPath.row].dt_txt?.split(separator: " ")[0].replacingOccurrences(of: "-", with: "/")
                cell.icon.image = UIImage(named:(Definations.SELECTED_CITIES_CURRENT_FORECAST[indexPath.row].weather?[0].icon)!)
                
                cell.temp.text = "\(Int((Definations.SELECTED_CITIES_CURRENT_FORECAST[indexPath.row].main?.temp)!))°C"
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == searchTV {
            Definations.CHOOSEN_CITIES.append(Definations.ALL_CITIES[indexPath.row])
            Definations.SELECTED_CITIES_CURRENT_FORECAST.removeAll()
            self.networking()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didCitiesChanged"), object: nil, userInfo: nil)
            Definations.ALL_CITIES.remove(at: indexPath.row)
            self.searchTV.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == searchTV {
            return 44
        }else{
            return 82
        }
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            Definations.SELECTED_CITIES_CURRENT_FORECAST.remove(at: indexPath.row)
            Definations.CHOOSEN_CITIES.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didCitiesChanged"), object: nil, userInfo: nil)
        }
        
    }
//    FIX ME:
//    While deleting a city from list, sometimes throws this error and app crashes.,
//    Terminating app due to uncaught exception 'CALayerInvalidGeometry', reason: 'CALayer bounds contains NaN: [nan 0; 396 35.3333].
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String?
    {
         return "Sil"
    }
    
}

