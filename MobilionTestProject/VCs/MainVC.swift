//
//  ViewController.swift
//  MobilionTestProject
//
//  Created by Kamil Tolkido on 9.01.2021.
//

import UIKit
import SJFluidSegmentedControl

class MainVC: UIViewController {
  
    @IBOutlet weak var weeklyForecastTV: UITableView!
    @IBOutlet weak var hourlyForecastCV: UICollectionView!
    @IBOutlet weak var currentForecastView: UIView!
    @IBOutlet weak var choosenCitiesSegmentedCOntrol: SJFluidSegmentedControl!
    
    @IBOutlet weak var currentTempIcon: UIImageView!
    @IBOutlet weak var currentTemp: UILabel!
    
    @IBOutlet weak var currentTempString: UILabel!
    
    @IBOutlet weak var dailyDetailView: UIView!
    @IBOutlet weak var dateOfCurrentTemp: UILabel!
    @IBOutlet weak var maxTempCurretn: UILabel!
    @IBOutlet weak var minTempCurrent: UILabel!
    
    @IBOutlet weak var uvLabel: UILabel!
    @IBOutlet weak var visibilityLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        networking()
        UISetup()
        self.choosenCitiesSegmentedCOntrol.delegate = self
        self.choosenCitiesSegmentedCOntrol.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didCitiesChanged(notification:)), name: NSNotification.Name(rawValue: "didCitiesChanged"), object: nil)
       
    }
    
    @objc func didCitiesChanged(notification: NSNotification) {
        networking()
    
        choosenCitiesSegmentedCOntrol.reloadData()
        choosenCitiesSegmentedCOntrol.layer.setNeedsLayout()
        choosenCitiesSegmentedCOntrol.setCurrentSegmentIndex(0, animated: true)
    }
    
//    MARK: UISetup
    fileprivate func UISetup(){
        self.currentTemp.font = UIFont.robotoRegular40
        self.currentTempString.font = UIFont.textStyle8
        self.dateOfCurrentTemp.font = UIFont.textStyle2
        
        self.maxTempCurretn.font = UIFont.textStyle6
        self.minTempCurrent.font = UIFont.textStyle6
        
        choosenCitiesSegmentedCOntrol.layer.cornerRadius = choosenCitiesSegmentedCOntrol.frame.height / 2
        
        currentForecastView.layer.applyCornerRadiusShadow()
        dailyDetailView.layer.applyCornerRadiusShadow()
        weeklyForecastTV.layer.applyCornerRadiusShadow()
    }
    
//    MARK: Networking
    fileprivate func networking() {
        Networking().getForecastById(cityId: String(Definations.CHOOSEN_CITIES[0].id)) { (err, forecast) in
            Definations.ALL_FORECAST = forecast.list!
            Definations.CURRENT_FORECST = forecast.list?[0]
            UIView.animate(withDuration: 0.5) {
                self.weeklyForecastTV.reloadData()
                self.currentForecastDetailUI()
                self.hourlyForecastCV.reloadData()
            }
        }
    }
    
//    MARK: Current Forecast UI
    fileprivate func currentForecastDetailUI() {
        if let currentTemp = Definations.CURRENT_FORECST?.main?.temp {
            self.currentTemp.text = "\(Int(currentTemp)) °C"
        }
        
        if let currentTempDesc = Definations.CURRENT_FORECST?.weather?[0].description {
            self.currentTempString.text = currentTempDesc.capitalized
        }
      
        if let maxTemp = Definations.CURRENT_FORECST?.main?.temp_max {
            self.maxTempCurretn.text = "\(Int(maxTemp)) °C"
        }
        
        if let minTemp = Definations.CURRENT_FORECST?.main?.temp_min {
            self.minTempCurrent.text = "\(Int(minTemp)) °C"
        }
        
        if let date = Definations.CURRENT_FORECST?.dt_txt {
            self.dateOfCurrentTemp.text = String(date.split(separator: " ")[0]).replacingOccurrences(of: "-", with: "/")
        }
        
        if let humidity = Definations.CURRENT_FORECST?.main?.humidity {
            self.humidityLabel.text = "% \(humidity)"
        }
        
        if let wind = Definations.CURRENT_FORECST?.wind?.speed {
            self.windLabel.text = "\(wind) mhs/s"
        }
        
        if let visibility = Definations.CURRENT_FORECST?.visibility{
            self.visibilityLabel.text = String(visibility)
        }
        
        if let uv = Definations.CURRENT_FORECST?.main?.temp_kf {
            self.uvLabel.text = String(uv)
        }
         
        UIView.animate(withDuration: 0.5) {
            if let icon = Definations.CURRENT_FORECST?.weather?[0].icon {
                self.currentTempIcon.image = UIImage(named: icon)
            }
            
        }
    }

    
//    MARK: Create Five Day Forecast
    fileprivate func createFiveDayForecast() -> [WeeklyForecastViewModel] {
        var weeklyForecast = [WeeklyForecastViewModel]()
        var totalMinTempForDay = 0
        var totalMaxTempForDay = 0
        var iconForDay = ""
        var dayName = ""
        var counterForDay = 0
        
        for forecast in Definations.ALL_FORECAST {
            if let date = Definations.CURRENT_FORECST?.dt_txt {
                if forecast.dt_txt?.split(separator: " ")[0] != date.split(separator: " ")[0] {
                    if counterForDay <= 7 {
                        if counterForDay == 0 {
                            dayName = getDayOfWeek(String((forecast.dt_txt?.split(separator: " ")[0])!))!

                            iconForDay = (forecast.weather?[0].icon)!
                        }
                        totalMinTempForDay += Int(forecast.main?.temp_min ?? 0)
                        totalMaxTempForDay += Int(forecast.main?.temp_max ?? 0)
                        counterForDay += 1
                    }else{
                        weeklyForecast.append(WeeklyForecastViewModel(dayName: dayName,
                                                                      maxTemp: totalMaxTempForDay/8,
                                                                      minTemp: totalMinTempForDay/8, icon: iconForDay))
                        
                        totalMinTempForDay = 0
                        totalMaxTempForDay = 0
                        counterForDay = 0
                    }
                    
                }
            }
        }
        
        return weeklyForecast
    }
    
// https://stackoverflow.com/questions/33719603/better-way-to-get-the-name-of-the-day-on-ios-swift
    func getDayOfWeek(_ today:String) -> String? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "tr_TR") as Locale
        dateFormatter.dateFormat = "EEEE"
        let dayInWeek = dateFormatter.string(from: todayDate)
        return dayInWeek
    }
    
}

// I use segmentedControl for listing selected cities but it could be UICollectionView too.
// MARK: SegmentedControl
extension MainVC: SJFluidSegmentedControlDataSource, SJFluidSegmentedControlDelegate {
    func numberOfSegmentsInSegmentedControl(_ segmentedControl: SJFluidSegmentedControl) -> Int {
        return Definations.CHOOSEN_CITIES.count
    }
    
    func segmentedControl(_ segmentedControl: SJFluidSegmentedControl, titleForSegmentAtIndex index: Int) -> String? {
        return Definations.CHOOSEN_CITIES[index].name
    }
    
    func segmentedControl(_ segmentedControl: SJFluidSegmentedControl, didChangeFromSegmentAtIndex fromIndex: Int, toSegmentAtIndex toIndex: Int) {
        Networking().getForecastById(cityId: String(Definations.CHOOSEN_CITIES[toIndex].id)) { (err, forecast) in
            Definations.ALL_FORECAST = forecast.list!
            Definations.CURRENT_FORECST = forecast.list?[0]
            UIView.animate(withDuration: 0.5) {
                self.weeklyForecastTV.reloadData()
                self.currentForecastDetailUI()
                self.hourlyForecastCV.reloadData()
            }
        }
    }
    
}


// MARK: UICollectionViewDelegate, DataSource, DelegateFlowLayout
extension MainVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "hourlyForecastCell", for: indexPath) as! HourlyForecastCVC
        
        cell.imageBackroundView.layer.cornerRadius = 12
        
        if indexPath.row < 5 && Definations.ALL_FORECAST.count != 0 {
            if let icon = Definations.ALL_FORECAST[indexPath.row].weather?[0].icon {
                cell.imageView.image = UIImage(named: icon)
            }
       
            if let hour = Definations.ALL_FORECAST[indexPath.row].dt_txt{
                cell.hourLabel.text = String(hour.dropLast(3).split(separator: " ")[1])
         
            }
        }
   
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

            let layout       = hourlyForecastCV.collectionViewLayout as? UICollectionViewFlowLayout
            let itemWidth    = hourlyForecastCV.bounds.width / 5.5
            let itemHeight   = hourlyForecastCV.bounds.height
            layout!.itemSize = CGSize(width: itemWidth, height: itemHeight)
            layout!.invalidateLayout()
            return CGSize(width: itemWidth, height: itemHeight)
    }
    
}

// MARK: UITableViewDelegate, DataSource
extension MainVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return createFiveDayForecast().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "weeklyForecastCell", for: indexPath) as! WeeklyForecastTVC
        
        if Definations.ALL_FORECAST.count != 0 {
            let weeklyForecast = createFiveDayForecast()
            
            cell.dayName.text = weeklyForecast[indexPath.row].dayName
            cell.icon.image = UIImage(named: weeklyForecast[indexPath.row].icon)
            cell.maxTemp.text = "\(weeklyForecast[indexPath.row].maxTemp)°"
            cell.minTemp.text = "\(weeklyForecast[indexPath.row].minTemp)°"
        }
 
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 42
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    
}
