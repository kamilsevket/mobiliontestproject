//
//  ChooseCityVC.swift
//  MobilionTestProject
//
//  Created by Kamil Tolkido on 9.01.2021.
//

import UIKit
import DropDown

class ChooseCityVC: UIViewController {
    
    @IBOutlet weak var continiueButton: UIButton!
    @IBOutlet weak var choosenCitiesTV: UITableView!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    let dropDown = DropDown()
    let footerView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        networking()
        UISetup()
    }
    

    fileprivate func networking(){
        Networking().getCityList { (err, citys) in }
    }
    
    fileprivate func UISetup(){
        continiueButton.isUserInteractionEnabled = false
        footerView.backgroundColor = .clear
        bottomView.layer.cornerRadius = 24
        bottomView.clipsToBounds = true
    }
    
//    MARK: DropDown Menu
//    I could use UITableView for showing all cities.
    fileprivate func dropMenuConfigure(){

        // The view to which the drop down will appear on
        dropDown.anchorView = dropDownView
        dropDown.width = self.bottomView.frame.width
    
        dropDown.dismissMode = .automatic
        
        DropDown.appearance().cornerRadius = 12
       
        dropDown.dataSource.removeAll()
        for cityName in Definations.ALL_CITIES {
            dropDown.dataSource.append(cityName.name)
        }
        dropDown.cellNib = UINib(nibName: "DropCell", bundle: nil)

        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard cell is DropCell else { return }
        }
        
        DropDown.appearance().textColor = UIColor.slate
        dropDown.show()
        
        dropDown.selectionAction = { [weak self] (index, item) in
            Definations.CHOOSEN_CITIES.append(Definations.ALL_CITIES[index])
            self!.dropDown.dataSource.remove(at: index)
            Definations.ALL_CITIES.remove(at: index)
            self!.choosenCitiesTV.reloadData()
            if Definations.CHOOSEN_CITIES.count >= 3 {
                UIView.animate(withDuration: 0.5) {
                    self?.continiueButton.isUserInteractionEnabled = true
                    self?.continiueButton.setTitleColor(.purplyBlue, for: .normal)
                }
            }
        }
    }

    @IBAction func chooseCityAction(_ sender: Any) {
        self.dropMenuConfigure()

    }
}


//  MARK: UITableViewDelegete, DataSource
extension ChooseCityVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Definations.CHOOSEN_CITIES.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cityCell", for: indexPath) as! ChoosenCityTVC
        
        cell.cityName.text = Definations.CHOOSEN_CITIES[indexPath.row].name

        return cell
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerView
    }

}

class DropCell: DropDownCell {
    
    
}
