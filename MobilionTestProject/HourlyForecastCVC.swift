//
//  HourlyForecastCVC.swift
//  MobilionTestProject
//
//  Created by Kamil Tolkido on 9.01.2021.
//

import UIKit

class HourlyForecastCVC: UICollectionViewCell {
    
    @IBOutlet weak var imageBackroundView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var hourLabel: UILabel!
    
}
